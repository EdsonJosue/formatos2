const readline = require('readline');

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('What is your favorite food?', function(answer) {
  console.log('Oh, so your favorite food is ' + answer);
});
